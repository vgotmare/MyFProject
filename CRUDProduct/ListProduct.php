<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname="product";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

//$sql="select * from Make_Product";
//$select=$conn->query($sql);

$sql="select Make_Product.*,image.image_name from Make_Product left join image on Make_Product.id=image.product_id";
$select=$conn->query($sql);

?>




<html>
<head>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>


	<body>
		<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
				<th>Image</th>
                <th>Created_At</th>
                <th>Updated_At</th>
				<th>Action</th>
            </tr>
        </thead>			
			<?php  
			
				if(isset($select))
				{
					foreach($select as $record)
					{
			?>
			
			<tr>
				<td align="Center"><?php  echo $record['id'] ?></td>
				<td align="Center"><?php  echo $record['product_name'] ?></td>
				<td align="Center"><?php  echo $record['quantity'] ?></td>
				<td align="Center"><?php  echo $record['price'] ?></td>
				<td align="Center"><img src="Upload/<?php echo $record['image_name']?>" Height="50px" width="50px"></img></td>
				<td align="Center"><?php  echo $record['created_at'] ?></td>
				<td align="Center"><?php  echo $record['updated_at'] ?></td>
				<td align="Center"><a href="EditProduct.php?id=<?php  echo $record['id'] ?>">Edit</a>
								   <a href="DeleteProduct.php?id=<?php  echo $record['id'] ?>">Delete</a>
				</td>
							
			</tr>
				<?php
					}
				}

				?>
		</table>	




</body>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        
    } );
} );
</script>

<html>